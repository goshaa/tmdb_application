<?php

return [
    'api_key'  => env('TMDB_API_KEY'),
    'api_lang' => env('TMDB_API_LANG')
];
