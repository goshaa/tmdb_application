<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Director extends Model
{
    protected $table = 'director';
    protected $fillable = [
        'name',
        'tmdb_id',
        'biography',
        'birth_date',
    ];
}
