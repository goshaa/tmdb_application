<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $table = 'movie';
    protected $fillable = [
        'title',
        'length',
        'release_date',
        'overview',
        'poster_url',
        'tmdb_id',
        'tmdb_vote_average',
        'tmdb_vote_count',
        'tmdb_url',
        'director_id',
    ];
    
    public function genres()
    {
        return $this->belongsToMany(Genre::class, 'genre2movie', 'movie_id', 'genre_id');
    }
    
    public function director()
    {
        return $this->hasOne(Director::class, 'id', 'director_id');
    }
}
