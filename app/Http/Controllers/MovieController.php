<?php

namespace App\Http\Controllers;

use App\Movie;

class MovieController extends Controller
{
    public function index()
    {
        $movies = Movie::all();
        return view('movies', compact('movies'));
    }
    
    public function show($id)
    {
        $movie = Movie::whereKey($id)->with(['genres', 'director'])->first();
        return view('movie', compact('movie'));
    }
}
