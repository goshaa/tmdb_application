<?php

namespace App\Console\Commands;

use App\Director;
use App\Genre;
use App\Movie;
use Goshaa\Tmbd\TmdbApiClient;
use Illuminate\Console\Command;
use Symfony\Component\Console\Helper\ProgressBar;

class UpdateDatabaseCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:database';
    protected $count = 210;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Call the Tmdb api and update the database with fresh records';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $apiClient = new TmdbApiClient(config('tmdb.api_key'), config('tmdb.api_lang'));
        
        $this->output->writeln('Starting the update of the top movies');
        $bar = new ProgressBar($this->output, $this->count);
        $movieIds = collect($apiClient->getResults($this->count))->pluck('id');
        
        Movie::whereNotIn('tmdb_id', $movieIds)->delete();
        
        foreach ($movieIds as $movieId)
        {
            $bar->advance();
            
            try
            {
                $this->processMovie($apiClient, $movieId);
            }
            catch (\Exception $e)
            {
                $this->output->writeln($e->getMessage());
            }
        }
        
        $bar->finish();
        $this->output->writeln('');
        $this->output->writeln('Update finished');
        
        return true;
    }
    
    /**
     *
     * @param TmdbApiClient $apiClient
     * @param integer $movieId
     * @return void
     */
    private function processMovie(TmdbApiClient $apiClient, $movieId)
    {
        $details = $apiClient->getDetails($movieId);
        $credits = $apiClient->getCredits($movieId);
        $directorTmbdId = collect($credits->crew)->filter(
            function ($item)
            {
                return $item->job == 'Director';
            }
        )->first()->id;
        
        $posterUrl = $apiClient->getPosterUrl($details->poster_path);
        $tmdbUrl = $apiClient->getTmdbUrl($movieId);
        $director = Director::where(['tmdb_id' => $directorTmbdId])->first();
        
        if (!$director)
        {
            $directorData = $apiClient->getPerson($directorTmbdId);
            $director = Director::create(
                [
                    'name'       => $directorData->name,
                    'tmdb_id'    => $directorTmbdId,
                    'biography'  => $directorData->biography,
                    'birth_date' => $directorData->birthday,
                ]
            );
        }
        
        $movie = Movie::where(['tmdb_id' => $movieId])->first();
        $attributes = [
            'title'             => $details->title,
            'length'            => $details->runtime,
            'release_date'      => $details->release_date,
            'overview'          => $details->overview,
            'poster_url'        => $posterUrl,
            'tmdb_id'           => $movieId,
            'tmdb_vote_average' => $details->vote_average,
            'tmdb_vote_count'   => $details->vote_count,
            'tmdb_url'          => $tmdbUrl,
            'director_id'       => $director->id
        ];
        
        if ($movie)
        {
            $movie->update($attributes);
            $movie->genres()->detach();
        }
        else
        {
            $movie = Movie::create($attributes);
        }
        
        foreach ($details->genres as $genre)
        {
            $movie->genres()->attach(Genre::where(['tmdb_id' => $genre->id])->first()->id);
        }
    }
}
