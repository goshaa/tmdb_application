@extends('layout')

@section('content')
    <div class="container">
        <div class="nav">
            <a href="{{route('movies')}}" class="btn btn-warning">@lang('lang.back')</a>
        </div>
    </div>
    <div class="container detail-container">
        <table class="table table-striped table-bordered detail-table">
            <tr>
                <th>@lang('lang.id')</th>
                <td>{{$movie->id}}</td>
            </tr>
            <tr>
                <th>@lang('lang.length')</th>
                <td><a href="{{$movie->tmdb_url}}" target="_blank">{{$movie->title}}</a></td>
            </tr>
            <tr>
                <th>@lang('lang.poster')</th>
                <td><a href="{{$movie->poster_url}}" target="_blank"><img src="{{$movie->poster_url}}" width="100px"></a></td>
            </tr>
            <tr>
                <th>@lang('lang.length')</th>
                <td>{{$movie->length}}</td>
            </tr>
            <tr>
                <th>@lang('lang.genre')</th>
                <td>{{implode(', ', $movie->genres->pluck('name')->toArray())}}</td>
            </tr>
            <tr>
                <th>@lang('lang.release_date')</th>
                <td>{{$movie->release_date}}</td>
            </tr>
            <tr>
                <th>@lang('lang.overview')</th>
                <td>{{$movie->overview}}</td>
            </tr>
            <tr>
                <th>@lang('lang.tmdb') @lang('lang.id')</th>
                <td>{{$movie->tmdb_id}}</td>
            </tr>
            <tr>
                <th>@lang('lang.tmdb') @lang('lang.vote_avg')</th>
                <td>{{$movie->tmdb_vote_average}}</td>
            </tr>
            <tr>
                <th>@lang('lang.tmdb') @lang('lang.vote_count')</th>
                <td>{{$movie->tmdb_vote_count}}</td>
            </tr>
            <tr>
                <th>@lang('lang.director') @lang('lang.name')</th>
                <td>{{$movie->director->name}}</td>
            </tr>
            <tr>
                <th>@lang('lang.director') @lang('lang.tmdb') @lang('lang.id')</th>
                <td>{{$movie->director->tmdb_id}}</td>
            </tr>
            <tr>
                <th>@lang('lang.director') @lang('lang.bio')</th>
                <td>{{$movie->director->biography}}</td>
            </tr>
            <tr>
                <th>@lang('lang.director') @lang('lang.birth_date')</th>
                <td>{{$movie->director->birth_date}}</td>
            </tr>
        </table>
    </div>
@endsection
