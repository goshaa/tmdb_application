@extends('layout')

@section('content')
    <div class="container table-container">
        <table id="example" class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>@lang('lang.title')</th>
                    <th>@lang('lang.length')</th>
                    <th>@lang('lang.release_date')</th>
                    <th>@lang('lang.tmdb') @lang('lang.id')</th>
                    <th>@lang('lang.tmdb') @lang('lang.vote_avg')</th>
                    <th>@lang('lang.tmdb') @lang('lang.vote_count')</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($movies as $movie)
                    <tr>
                        <td>{{$movie->id}}</td>
                        <td>{{$movie->title}}</td>
                        <td>{{$movie->length}}</td>
                        <td>{{$movie->release_date}}</td>
                        <td>{{$movie->tmdb_id}}</td>
                        <td>{{$movie->tmdb_vote_average}}</td>
                        <td>{{$movie->tmdb_vote_count}}</td>
                        <td><a href="{{route('movie', ['id' => $movie->id])}}" class="btn btn-info">@lang('lang.details')</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
