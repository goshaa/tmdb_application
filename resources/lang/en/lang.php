<?php

return [
    'application_name' => 'TMDB application',
    'id'               => 'Id',
    'title'            => 'Title',
    'poster'           => 'Poster',
    'length'           => 'Length',
    'genre'            => 'Genre(s)',
    'release_date'     => 'Release date',
    'overview'         => 'Overview',
    'tmdb'             => 'TMDB',
    'vote_avg'         => 'vote average',
    'vote_count'       => 'vote count',
    'director'         => 'Director',
    'name'             => 'name',
    'bio'              => 'biography',
    'birth_date'       => 'date of birth',
    'back'             => 'Back',
    'details'          => 'Details',
];
