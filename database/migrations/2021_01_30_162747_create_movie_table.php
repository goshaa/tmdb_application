<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMovieTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movie', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->integer('length');
            $table->date('release_date');
            $table->text('overview')->nullable();
            $table->string('poster_url')->nullable();
            $table->integer('tmdb_id')->unique();
            $table->integer('tmdb_vote_average');
            $table->integer('tmdb_vote_count');
            $table->string('tmdb_url');
            $table->unsignedBigInteger('director_id')->nullable();
            $table->timestamps();

            $table->foreign('director_id')
                ->references('id')
                ->on('director')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movie');
    }
}
