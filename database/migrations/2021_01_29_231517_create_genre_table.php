<?php

use App\Genre;
use Goshaa\Tmbd\TmdbApiClient;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGenreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('genre', function (Blueprint $table)
        {
            $table->id();
            $table->integer('tmdb_id');
            $table->string('name');
            $table->timestamps();
        });
        
        $client = new TmdbApiClient(config('tmdb.api_key'), config('tmdb.api_lang'));
        
        foreach (collect($client->getGenres()) as $genre)
        {
            Genre::create(
                [
                    'tmdb_id' => $genre->id,
                    'name'    => $genre->name
                ]
            );
        }
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('genre');
    }
}
