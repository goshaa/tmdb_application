$(document).ready(function() {
    $('#example').DataTable({
        columnDefs: [
            { targets: [7], sortable: false}
        ],
        order: [[ 5, "desc" ]]
    });
});
